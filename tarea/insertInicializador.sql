INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (1, 'iphone', '50000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (2, 'cartera', '5000', '5');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (3, 'mochila', '10000', '15');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (4, 'audifonos', '4500', '13');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (5, 'notebook', '700000', '4');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (6, 'lentes', '5000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (7, 'taza', '1000', '60');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (8, 'mesa', '45000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (9, 'sillon', '60000', '11');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (10, 'escritorio', '30000', '9');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (11, 'silla', '5000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (12, 'cafe', '6000', '6');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (13, 'te', '5000', '8');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (14, 'cama', '5000', '9');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (15, 'tetera', '5000', '3');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (16, 'plato', '5000', '4');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (17, 'cuchara', '5000', '5');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (18, 'tenedor', '5000', '7');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (19, 'chuchillo', '5000', '3');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (20, 'cepillo', '5000', '4');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (21, 'peineta', '5000', '25');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (22, 'cubrecama', '5000', '30');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (23, 'sabana', '5000', '16');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (24, 'toalla', '5000', '17');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (25, 'encendedor', '500', '18');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (26, 'fosforos', '1000', '19');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (27, 'alicate', '1000', '20');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (28, 'tabaquera', '1000', '21');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (29, 'martillo', '1000', '22');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (30, 'cantimplora', '10000', '14');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (31, 'vino', '5000', '16');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (32, 'cerveza', '1000', '17');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (33, 'pisco', '5000', '50');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (34, 'vodka', '5000', '34');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (35, 'wiski', '5000', '23');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (36, 'tequila', '5000', '35');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (37, 'ron', '5000', '44');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (38, 'cerveza artesanal', '1000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (39, 'te yogi', '1000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (40, 'mate', '1000', '10');
INSERT INTO `proyecto_tarea`.`productos` (`id`, `nombre`, `precio`, `stock`) VALUES (41, 'televisor', '50000', '10');
INSERT INTO `proyecto_tarea`.`usuario` (`id`, `apellido`, `email`, `factura`, `nombre`, `password`, `rol`, `session_user`, `telefono`, `username`) VALUES (1, 'martinez', NULL, NULL, 'jorge', 'jorge', '1', '50', NULL, 'jorge')