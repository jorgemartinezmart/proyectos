package tinet.tutoriales;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CARRO_PRODUCTO")
public class CarroProducto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/**
	 * bi-directional many-to-one association to Carro.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCarro", referencedColumnName = "id")
	private Carro idCarro;

	/**
	 * bi-directional many-to-one association to Producto.
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idProducto", referencedColumnName = "id")
	private Productos idProducto;

	public CarroProducto() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Carro getIdCarro() {
		return idCarro;
	}

	public void setIdCarro(Carro idCarro) {
		this.idCarro = idCarro;
	}

	public Productos getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Productos idProducto) {
		this.idProducto = idProducto;
	}

}