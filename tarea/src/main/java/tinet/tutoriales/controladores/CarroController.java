package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.Carro;
import tinet.tutoriales.repositories.CarroProductoRepository;
import tinet.tutoriales.repositories.CarroRepository;

@RestController
public class CarroController {

	@Autowired
	CarroRepository carroRepository;
	CarroProductoRepository carroPructoRepository;
	
	 //guardar carro
	 @RequestMapping(value="/carro", method=RequestMethod.POST)
     public void addCarro(@RequestBody Carro c){		 
		 carroRepository.save(c);
     }
	 
	 //obtener carro
	 @RequestMapping(value="/carro/{id}", method=RequestMethod.GET)
     public Carro getCarro(@PathVariable long id){
		 return carroRepository.findOne(id);
     }
	 
	 //obtener todos los carros
	 @RequestMapping(value="/carro", method=RequestMethod.GET)
     public List<Carro> getCarros(){
		  List<Carro> findAll = carroRepository.findAll(); 
		  return findAll;
     }  
	 
	 
	 //eliminar carro
	 @RequestMapping(value="/carro/{id}", method=RequestMethod.DELETE)
	 public void deleteCarro(@PathVariable long id){
		 carroRepository.delete(id);
	 }     
	 
	//obtener ultimo carro		 
		 @RequestMapping("/getUltimoCarro")
		 public @ResponseBody int getUltimoCarro(){					
			 return carroRepository.getUltimoCarro();
		 }	 
}
