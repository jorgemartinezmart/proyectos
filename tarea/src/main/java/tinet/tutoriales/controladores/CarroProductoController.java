package tinet.tutoriales.controladores;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.CarroProducto;
import tinet.tutoriales.Productos;
import tinet.tutoriales.repositories.CarroProductoRepository;
import tinet.tutoriales.repositories.ProductosRepository;

@RestController
public class CarroProductoController {

	@Autowired
	CarroProductoRepository carroPructoRepository;
	@Autowired
	ProductosRepository productosRepository;

	// guarda el carro y actualiza stock
	@RequestMapping(value = "/carroProducto", method = RequestMethod.POST)
	public void addCarroProducto(@RequestBody CarroProducto c) {
		try {
			carroPructoRepository.save(c);
			Productos encontrado = productosRepository.findOne(c.getIdProducto().getId());
			encontrado.setStock(String
					.valueOf(Integer.parseInt(encontrado.getStock()) - Integer.parseInt(c.getIdProducto().getStock())));
			productosRepository.save(encontrado);
		} catch (Exception e) {
			System.out.println("Error");
		}
	}

}
