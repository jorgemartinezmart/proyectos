package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.Usuario;
import tinet.tutoriales.repositories.UsurariosRepository;

@RestController
public class UsuariosController {

	@Autowired
	UsurariosRepository usuariosRepository;

	// guardar usuario
	@RequestMapping(value = "/personas", method = RequestMethod.POST)
	public void addPersona(@RequestBody Usuario u) {
		usuariosRepository.save(u);
	}

	// obtener usuario
	@RequestMapping(value = "/personas/{id}", method = RequestMethod.GET)
	public Usuario obtenerPersona(@PathVariable long id) {
		return usuariosRepository.findOne(id);
	}

	// obtener todas los usuario
	@RequestMapping(value = "/personas", method = RequestMethod.GET)
	public List<Usuario> obtenerPersonas() {
		return usuariosRepository.findAll();
	}

	// eliminar usuario
	@RequestMapping(value = "/personas/{id}", method = RequestMethod.DELETE)
	public void eliminarPersona(@PathVariable long id) {
		usuariosRepository.delete(id);
	}

	// modificar usuario
	@RequestMapping(value = "/personas/{id}", method = RequestMethod.PUT)
	public void modificarPersona(@RequestBody Usuario u) {
		Usuario encontrado = usuariosRepository.findOne(u.getId());
		encontrado.setNombre(u.getNombre());
		encontrado.setEmail(u.getEmail());
		encontrado.setTelefono(u.getTelefono());
		usuariosRepository.save(encontrado);
	}

	// iniciar sesion
	@RequestMapping("/iniciarSesion")
	public @ResponseBody Usuario IniciarSesion(
			@RequestParam(value = "nombreUsuario", required = true) String nombreUsuario,
			@RequestParam(value = "clave", required = true) String clave) {
		Usuario usuario = new Usuario();
		try {
			System.out.println("Validando usuario");
			usuario = usuariosRepository.validaUsuario(nombreUsuario, clave);
			System.out.println("Creando session usuario");

			if (usuario.getSessionUser() == 0) {
				// se busca penúltimo usuario crear
				Usuario usuarioEncontrado = usuariosRepository.findOne(usuario.getId() - 1);
				// al usuario que se está logeando se le asocia la sesion del
				// usuario anterior pero incrementandola en 1
				usuario.setSessionUser(usuarioEncontrado.getSessionUser() + 1);
				usuariosRepository.save(usuario);
			} else {
				System.out.println("Usuario ya tiene sesion");
			}
		} catch (Exception e) {
			System.out.println("Error al iniciar y crear sesion");
		}

		return usuario;
	}

	// registrar usuario
	@RequestMapping("/registrarUsuario")
	public @ResponseBody Usuario registrarUsuario(@RequestParam(value = "nombre", required = true) String nombre,
			@RequestParam(value = "apellido", required = true) String apellido,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String clave) {
		Usuario usuario = new Usuario();
		Usuario user = new Usuario();
		System.out.println("Registrando usuario");
		usuario = usuariosRepository.validaUsuario(username, clave);

		if (usuario == null) {
			try {
				user.setNombre(nombre);
				user.setApellido(apellido);
				user.setUsername(username);
				user.setPassword(clave);
				user.setRol("2");
				usuariosRepository.save(user);
			} catch (Exception e) {
				System.out.println("Error al registrarse");
			}
		} else {
			System.out.println("Usuario ya existe");
		}
		return user;
	}
}
