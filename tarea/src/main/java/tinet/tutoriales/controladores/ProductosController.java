package tinet.tutoriales.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tinet.tutoriales.Productos;
import tinet.tutoriales.repositories.ProductosRepository;

/**
 * vhvjhvjhv jh
 * 
 * @author TInet
 *
 */
@RestController
public class ProductosController {

	@Autowired
	ProductosRepository productosRepository;

	// guardar producto
	@RequestMapping(value = "/productos", method = RequestMethod.POST)
	public void addProducto(@RequestBody Productos p) {
		productosRepository.save(p);
	}

	// obtener producto
	@RequestMapping(value = "/productos/{id}", method = RequestMethod.GET)
	public Productos obtenerProducto(@PathVariable long id) {
		return productosRepository.findOne(id);
	}

	// obtener todas los productos
	@RequestMapping(value = "/productos", method = RequestMethod.GET)
	public List<Productos> obtenerProductos() {
		List<Productos> findAll = productosRepository.findAll();
		return findAll;
	}

	// eliminar producto
	@RequestMapping(value = "/productos/{id}", method = RequestMethod.DELETE)
	public void eliminarProductos(@PathVariable long id) {
		productosRepository.delete(id);
	}


	// modificar STOCK producto
	@RequestMapping(value = "/productos/actualizarStock/", method = RequestMethod.PUT)
	public void actualizarStock(@RequestBody Productos p) {
		// productosRepository.actualizarStock(Integer.parseInt(p.getStock()),
		// p.getId());
		Productos encontrado = productosRepository.findOne(p.getId());
		encontrado.setStock(String.valueOf(Integer.parseInt(encontrado.getStock()) - Integer.parseInt(p.getStock())));
		productosRepository.save(encontrado);
	}

	// obtener todas los productos
	@RequestMapping(value = "/productos/obtenerRegistrosPaginados/", method = RequestMethod.GET)
	public List<Productos> obtenerProductosPaginados(@RequestParam long limite) {
		long inicio = 0;
		if (limite <= 0) {
			limite = 10;
			inicio = 0;
		} else {
			System.out.println("limite"+limite);
			inicio = limite - 10;
			System.out.println("inicio"+inicio);
		}
		List<Productos> findAll = productosRepository.getRegistrosPaginados(limite, inicio);
		return findAll;
	}
}
