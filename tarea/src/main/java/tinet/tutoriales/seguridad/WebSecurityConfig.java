package tinet.tutoriales.seguridad;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

//@Configuration
//@EnableWebSecurity
//
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//	@Autowired
//	DataSource dataSource;
//
////	@Autowired
////	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
////
////	  auth.jdbcAuthentication().dataSource(dataSource)
////		.usersByUsernameQuery(
////			"select username,password, enabled from usuario where username=?")
////		.authoritiesByUsernameQuery(
////			"select username, rol from usuario where username=?");
////	}
//
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		   http
//		      .httpBasic().and()
//		      .authorizeRequests()
//		        //.antMatchers("/app/index.html", "/app/home.html", "/app/login.html", "/").permitAll().anyRequest()
//		      .antMatchers("/app/index.html", "/**/*.js", "/app/login/login.html").permitAll().anyRequest()
//		        .authenticated().and().csrf()
//                .csrfTokenRepository(csrfTokenRepository()).and()
//                .addFilterAfter(new CsrfHeadFilter(), CsrfFilter.class)
//                .logout();
//		  }
//	
//	private CsrfTokenRepository csrfTokenRepository() {
//		  HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
//		  repository.setHeaderName("X-XSRF-TOKEN");
//		  return repository;
//		}
//	
//	 @Bean
//	    public ObjectMapper jacksonObjectMapper() {
//	        return new CustomObjectMapper();
//	    }   
//}
@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().authorizeRequests()
				// se permiten ingresar sin autorizacion a los siguientes html's
				// i a todos los archivos javascript
				.antMatchers("/app/index.html", "/**/*.js", "/app/login/login.html").permitAll()
				// esto se debe hacer ya que Spring-security tine integrada la
				// seguridad
				// contra atques del tipo CSRF, basicamente en cada solicitud se
				// envia un token de verificacion
				// si no se recive el mismo toquen
				.anyRequest().authenticated().and().csrf().csrfTokenRepository(csrfTokenRepository()).and()
				.addFilterAfter(new CsrfHeadFilter(), CsrfFilter.class);
	}

	private CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		return repository;
	}

	@Bean
	public ObjectMapper jacksonObjectMapper() {
		return new CustomObjectMapper();
	}
}
