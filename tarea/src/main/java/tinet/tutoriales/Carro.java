package tinet.tutoriales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Carro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "nombre_carro")
	private String nombreCarro;
	private String session;
	private String usuario;
	private String estado;
	private boolean clearCart;

	public void setId(long id) {
		this.id = id;
	}

	public String getNombreCarro() {
		return nombreCarro;
	}

	public void setNombreCarro(String nombreCarro) {
		this.nombreCarro = nombreCarro;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public boolean isClearCart() {
		return clearCart;
	}

	public void setClearCart(boolean clearCart) {
		this.clearCart = clearCart;
	}

}