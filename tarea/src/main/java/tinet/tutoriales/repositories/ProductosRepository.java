package tinet.tutoriales.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tinet.tutoriales.Productos;

public interface ProductosRepository extends JpaRepository<Productos, Long> {
	@Query(value = "UPDATE PRODUCTOS SET (STOCK = (SOTCK - :stock) WHERE ID = :idProducto", nativeQuery = true)
	long actualizarStock(@Param(value = "stock") long stock, @Param(value = "idProducto") long idProducto);

	@Query(value = "SELECT * FROM PRODUCTOS WHERE STOCK > 0 LIMIT :limite OFFSET :inicio", nativeQuery = true)
	List<Productos> getRegistrosPaginados(@Param(value = "limite") long limite, @Param(value = "inicio") long inicio);

}