package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tinet.tutoriales.Usuario;

public interface UsurariosRepository extends JpaRepository<Usuario, Long> {
	@Query(value = "SELECT * FROM USUARIO WHERE USERNAME = :username", nativeQuery = true)
	Usuario findByName(@Param(value = "username") String username);

	@Query(value = "SELECT * FROM USUARIO WHERE USERNAME = :username AND PASSWORD = :clave", nativeQuery = true)
	Usuario validaUsuario(@Param(value = "username") String username, @Param(value = "clave") String clave);

}
