package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tinet.tutoriales.Carro;

public interface CarroRepository extends JpaRepository<Carro, Long> {

	@Query(value = "SELECT MAX(ID) FROM CARRO", nativeQuery = true)
	int getUltimoCarro();
}
