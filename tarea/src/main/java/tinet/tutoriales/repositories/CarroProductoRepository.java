package tinet.tutoriales.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tinet.tutoriales.CarroProducto;

public interface CarroProductoRepository extends JpaRepository<CarroProducto, Long> {
	@Query(value = "INSERT INTO CARRO_PRODUCTO VALUES(:id, :idCarro, :idProducto )", nativeQuery = true)
	long insertCarroProductos(@Param(value = "id") long id, @Param(value = "idCarro") long idCarro,
			@Param(value = "idProducto") long idProducto);
}