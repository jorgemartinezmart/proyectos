angular.module("myApp").factory('ProductoFactory', ProductoFactory);
// Declaracion de recursos que se deben inyectar.
ProductoFactory.$inject = [ '$resource' ];
function ProductoFactory($resource) {
	return $resource('/productos/:id', {
		id : '@id'
	}, {
		'edit' : {
			method : 'PUT'
		}
	})
}