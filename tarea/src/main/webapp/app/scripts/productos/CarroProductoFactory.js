angular.module("myApp").factory('CarroProductoFactory', CarroProductoFactory);
CarroProductoFactory.$inject = [ '$resource' ];
function CarroProductoFactory($resource) {
	return $resource('/carroProducto/:id', {
		id : '@id'
	}, {
		'edit' : {
			method : 'PUT'
		}
	})
}