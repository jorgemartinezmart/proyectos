angular.module("myApp")
.controller('ModificarProductoController',modificarProducto);

//Esta línea sirve para declarar los objetos que se inyectarán al controller
modificarProducto.$inject = ['$scope', 'MantenedorProductoFactory','$stateParams','$state'];
//Definicion del controller
function modificarProducto($scope, MantenedorProducto, $stateParams, $state){
       //Objeto para contener datos del formulario de creacion
       
	$scope.formEditar = {};
	$scope.obtenerUsuario =
       	function(){
    	   Usuario.get({id: $stateParams.id},
    			   function(aux){
    		   $scope.formEditar =aux;
    	   },function(){
    		   console.log("error")
    	   }
    	   )
       };
       $scope.obtenerUsuario();
       $scope.listado = [];
       $scope.editarUsuario = function(){
    	  MantenedorProducto.edit({id: $scope.formEditar.id},$scope.formEditar);
          refrescarlistado();
          $state.go('app');         
     }
       
       //Se Realizan las llamadas que obtienen el listado personas
       //Se declaran funciones utilitarias que solo se pueden usar en este controller.
       function refrescarlistado(){
             $scope.listado = MantenedorProducto.query();
       }
       
       
       
       /*function obtenerSalas(){
             $scope.salas = GestionSalas.query();
       } */       
};       