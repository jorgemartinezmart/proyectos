angular.module("myApp")
.controller('PrincipalProductoController',principalProducto);

//Esta línea sirve para declarar los objetos que se inyectarán al controller
principalProducto.$inject = ['$scope', 'MantenedorProductoFactory','$stateParams'/*,'$http'*/];
//Definicion del controller
function principalProducto($scope, MantenedorProducto, $stateParams/*, $http*/){
       //Objeto para contener datos del formulario de creacion
       $scope.listado = [];
       
       
       //Se Realizan las llamadas que obtienen el listado personas
       refrescarlistado();
       
       $scope.eliminarPersona = function(data){
           data.$delete(refrescarlistado);
       } 
       //Se declaran funciones utilitarias que solo se pueden usar en este controller.
       function refrescarlistado(){
             $scope.listado = MantenedorProducto.query();
       }       
       /*function obtenerSalas(){
             $scope.salas = GestionSalas.query();
       } */       
};       