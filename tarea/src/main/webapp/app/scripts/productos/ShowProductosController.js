angular.module("myApp")
.controller('ShowProductosController',ShowProductosController);

//Esta línea sirve para declarar los objetos que se inyectarán al controller
ShowProductosController.$inject = ['$scope', 'ProductoFactory','$stateParams','$location', '$routeParams'];
//Definicion del controller
function ShowProductosController($scope, Producto, $stateParams, $location, $routeParams){
       $scope.listado = [];            
       refrescarlistado();
                
       $scope.carro = DataService.carro;
       
       // use routing to pick the selected product
       if ($routeParams.productStock!= null) {
           $scope.product = $scope.store.getProduct($routeParams.productStock);
       }
       
       function refrescarlistado(){
             $scope.listado = Producto.query();
       }       
};       