﻿angular.module("myApp")
.controller('carroController',carroController)
carroController.$inject = ['$scope', '$routeParams','DataService','ProductoFactory', 'CarroProductoFactory' ,'CarroFactory', '$stateParams','$location', '$http', 'LoginService'];

function carroController($scope, $routeParams, DataService, Producto, CarroProducto , Carro, $stateParams, $location, $http, LoginService) {
	$scope.listado = [];            
    refrescarlistado();
	/**
	 * obteniendo store y carro desde el servicio DataService
	 * 
	 */ 
    
 $scope.store = DataService.store;
 $scope.carro = DataService.carro;
		// si esta logeado crea carro vacio, si no busca el carro asociado al
		// usuario logeado
//		if($scope.usrConectado.estaConectado == true){
//					$scope.carro = DataService.carro;
//					$scope.carro.loadItemsBySession($scope.usrConectado.sessionId);
//					// $scope.store.agregarCarro($scope.carrito,
//					// $scope.carritoProductos);
//		};// else{
// };
		
		$scope.comprar = function (carrito){
			try{
				 if($scope.usrConectado.estaConectado == true){
					 $scope.sesion = $scope.usrConectado.sessionId;
				    	// agregar sesion y usuario a carrito
				    	// carrito = carrito.items;
				    	carro = {  
				    		 'nombreCarro': carrito.nombreCarro,
				             'clearCart': carrito.clearCart,
				             'session': $scope.sesion,
				             'usuario': carrito.usuario,
				             'estado' : 'FINALIZADO'
				    	};    	
				    	carroBD = new Carro(carro);
				    	$scope.carrito = carrito;
				    	$scope.carritoProductos = carrito.items;
		
				    	/**
						 * Funcion save, guarda carro en tabla carro, luego guarda en
						 * tabla intermediara CARRO_PRODUCTO los registros para asociar
						 * carro con productos, luego se debe actualizar los stock de
						 * cada producto. Se agrega carro listo a la lista de
						 * carros(Store) y se limpian las variables
						 * 
						 */
				    	
					    carroBD.$save(function() {	
					    		guardarEnCarroProducto($scope.carrito.items);
					    		$scope.store.agregarCarro($scope.carrito, $scope.carritoProductos);
					    		$scope.carro.clearItems();
					    		$scope.carro = DataService.carro;
					    		$location.path('/productos');
					    	},(function() {
					    		(console.log("error"));
					    		}
					    	 )
					      );
				 }else{
					 alert("Debe conectarse");
					 $location.path('/inicio');
				 }
			}catch (err) {
				console.log("Error " + err );
			}	 
    	}
    
    function guardarEnCarroProducto(carrito){
  			$http.get('http://localhost:8080/getUltimoCarro').then(function(id){
  				idProductos = carrito;
  			for (var i = 0; i < idProductos.length; i++) {
                var item = idProductos[i];
                // obteniendo cantidad total por cada producto
                var cantidadTotal=0;
                for(var f = 0; f < idProductos.length; f++){
                	var prod = idProductos[f];   
                	if(prod.nombre == item.nombre){
                		cantidadTotal = cantidadTotal + prod.cantidad;
                	}
                }
                 for(var j = 0; j < item.cantidad; j++){
		                datos =
		                		{          				
		                		  'idCarro' : carro = {'id':id.data},
		                		  'idProducto' : producto = {'id':item.id, 'stock' : cantidadTotal}
		                		};
		                	console.log("datos"+datos);
		          			carroProducto = new CarroProducto(datos);
		          			carroProducto.$save();
		          			console.log("carro numero " + id.data + " producto numero " + i + " guardado");
		          }
            }
  		},
  			function(){
  				console.log("error")
  			});
    }
    
    
    /**
	 * usar routing para elegir producto seleccionado
	 * 
	 */
    
//    if ($routeParams.productStock!= null) {
//        $scope.product = $scope.store.getProduct($routeParams.productStock);
//    }
    
    // paginación para avanzar
    $scope.avanzar = function(){
    	$scope.cantidadRegistros = $scope.cantidadRegistros + 10;
    	refrescarlistado();
    }
    // paginación para retroceder
    $scope.retroceder = function(){
    	$scope.cantidadRegistros = $scope.cantidadRegistros - 10;
    	refrescarlistado();
    }
    
    function refrescarlistado(){
    	limite=$scope.cantidadRegistros;
    	$http.get('http://localhost:8080/productos/obtenerRegistrosPaginados/',{params: {limite}}).then(function(listaPaginada){
  			listaPaginada = listaPaginada.data;
  			console.log(listaPaginada);
  			$scope.listado = [];
  			$scope.listado = listaPaginada;
  	    },
  			function(){
  				console.log("error")
  			});          
    }  
};
