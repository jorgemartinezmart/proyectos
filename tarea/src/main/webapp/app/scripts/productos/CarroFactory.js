angular.module("myApp").factory('CarroFactory', CarroFactory);
CarroFactory.$inject = [ '$resource' ];
function CarroFactory($resource) {
	return $resource('/carro/:id', {
		id : '@id'
	}, {
		'edit' : {
			method : 'PUT'
		}
	})
}