﻿//----------------------------------------------------------------
// Carro de compra
//
function shoppingCart() {
	this.nombreCarro = "Carro";
	this.clearCart = false;
	this.idSessionCarro = "";// this.secuencialSessionCarro();
	this.usuario = "";
	this.estado = "Incompleto";
	this.items = [];
	
	this.loadItems();

    var self = this;
    $(window).unload(function () {
        if (self.clearCart) {
            self.clearItems();
        }
        self.saveItems();
        self.clearCart = false;
    });
}

shoppingCart.prototype.obtenerUltimoSecuencialSessionId = function() {
	productos = localStorage.getItem("Carro_items");
	productos = JSON.parse(productos);
	for (var i = 0; i < productos.length; i++) {
		prod = productos[i];
		console.log(prod);
	}
}

// cargar items del local storage
shoppingCart.prototype.loadItems = function() {
	var items = localStorage != null ? localStorage[this.nombreCarro + "_items"]
			: null;
	if (items != null && JSON != null) {
		try {
			var items = JSON.parse(items);
			// if(){
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				if (item.id != null && item.nombre != null
						&& item.precio != null && item.stock != null
						&& item.cantidad != null) {
					item = new cartItem(item.id, item.nombre, item.precio,
							item.stock, item.cantidad);
					this.items.push(item);
				}
			}
			// }
		} catch (err) {
			// ignore errors while loading...
		}
	}
}

// cargar items de carro asociado a usuario segun sesion
shoppingCart.prototype.loadItemsBySession = function(session) {
	console.log("hola estoy conectado");
	console.log(session);
	var items = localStorage != null ? localStorage["Carro_items"] : null;
	console.log(items);
	if (items != null && JSON != null) {
		try {
			var carros = JSON.parse(items);
			console.log(carros);
			for (var i = 0; i < carros.length; i++) {
				var listaProdutos = carros[i];
				var listaProdutos = JSON.parse(listaProdutos);
				for (var i = 0; i < listaProdutos.length; i++) {
					var producto = listaProdutos[i];
					if (producto.idSessionCarro != null
							&& producto.idSessionCarro == session
							&& producto.id != null && producto.nombre != null
							&& producto.precio != null
							&& producto.stock != null
							&& producto.cantidad != null) {
						producto = new cartItem(producto.id, producto.nombre,
								producto.precio, producto.stock,
								producto.cantidad);
						this.items.push(producto);
					}
				}
			}
		} catch (err) {
			// ignore errors while loading...
		}
	}

	var self = this;
	$(window).unload(function() {
		if (self.clearCart) {
			self.clearItems();
		}
		self.saveItems();
		self.clearCart = false;
	});
}

// mostrar local storage
shoppingCart.prototype.mostrarTodo = function() {
	for (var i = 0, t = localStorage.length; i < t; i++) {
		key = localStorage.key(i);
		console.log('Para la clave ' + key + ' el valor es: '
				+ localStorage[key]);
	}
	;
}

// mostrar productos del carro
shoppingCart.prototype.mostrarCarros = function() {
	productos = localStorage.getItem("Carro_items");
	productos = JSON.parse(productos);
	for (var i = 0; i < productos.length; i++) {
		prod = productos[i];
		console.log(prod);
	}
}

// guardando item a local storage
shoppingCart.prototype.saveItems = function() {
	if (localStorage != null && JSON != null) {
		localStorage[this.nombreCarro + "_items"] = JSON.stringify(this.items);
	}
}

// agregar un item(producto) al carro
shoppingCart.prototype.agregarProducto = function(id, nombre, precio, stock,
		cantidad) {
	if (cantidad != 0) {
		// actualizar cantidad para item existente
		var found = false;
		for (var i = 0; i < this.items.length && !found; i++) {
			var item = this.items[i];
			if (item.stock == stock) {
				found = true;
				item.cantidad = (item.cantidad + cantidad);
				if (item.cantidad <= 0) {
					this.items.splice(i, 1);
				}
			}
		}

		// agregar nuevo item
		if (!found) {
			var item = new cartItem(id, nombre, precio, stock, cantidad);
			this.items.push(item);
		}

		// guardar cambios
		this.saveItems();
	}
}

// obtener el precio total para todos los items actuales en el carro
shoppingCart.prototype.getPrecioTotal = function(stock) {
	var total = 0;
	for (var i = 0; i < this.items.length; i++) {
		var item = this.items[i];
		if (stock == null || item.stock == stock) {
			total += (item.precio * item.cantidad);
		}
	}
	return total;
}

// obtener la cantidad de items actuales en el carro
shoppingCart.prototype.getTotal = function(nombre) {
	var count = 0;
	for (var i = 0; i < this.items.length; i++) {
		var item = this.items[i];
		if (nombre == null || item.nombre == nombre) {
			// count += this.toNumber(item.cantidad);
			count += (item.cantidad);
		}
	}
	return count;
}

// limpiar el carro
shoppingCart.prototype.clearItems = function() {
	this.items = [];
	this.saveItems();
}

// limpiar todo el local storage
shoppingCart.prototype.clearLocalStorage = function() {
	if (localStorage != null) {
		for (var i = 0, t = localStorage.length; i < t; i++) {
			console.log("Eliminando " + localStorage.key(i));
			localStorage.removeItem(localStorage.key(i));
		}
	} else {
		alert("LocalStorage vacío");
	}
}

// ----------------------------------------------------------------
// productos en el carro
// */
function cartItem(id, nombre, precio, stock, cantidad) {
	this.id = id;
	this.nombre = nombre;
	this.precio = precio;
	this.stock = stock;
	this.cantidad = cantidad;
}
