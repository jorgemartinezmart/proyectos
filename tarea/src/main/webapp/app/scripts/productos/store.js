﻿//----------------------------------------------------------------
// store (contenedor de carros)
//
function store() {
	this.nombreCarro = "ListaCarro";
	this.clearCart = false;
	this.usuario = "";
	this.estado = "Incompleto";
	this.items = [];

	 this.loadItems();
	 // guardar items en local storage cuando carga
	 var self = this;
	 $(window).unload(function () {
	 if (self.clearCart) {
	 self.clearItems();
	 }
	 self.saveItems();
	 self.clearCart = false;
	 });
}

store.prototype.obtenerCarroUsuario = function(sessionUser) {
	// cargar items del local storage segun session de usuario
	var items = localStorage != null ? localStorage[this.nombreCarro + "_items"]
			: null;
	if (items != null && JSON != null) {
		try {
			var items = JSON.parse(items);
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				if (item.carrito != null && item.listaProductos != null) {
					item = new itemCarro(item.carrito, item.listaProductos);
					this.items.push(item);
				}
			}
		} catch (err) {
			// ignore errors while loading...
		}
		// guardar items en local storage cuando carga
		var self = this;
		$(window).unload(function() {
			if (self.clearCart) {
				self.clearItems();
			}
			self.saveItems();
			self.clearCart = false;
		});

	}
}
store.prototype.getProduct = function(stock) {
	for (var i = 0; i < this.products.length; i++) {
		if (this.products[i].stock == stock)
			return this.products[i];
	}
	return null;
}

// cargar carros del local storage
store.prototype.loadItems = function() {
	var items = localStorage != null ? localStorage[this.nombreCarro + "_items"]
			: null;
	if (items != null && JSON != null) {
		try {
			var items = JSON.parse(items);
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				if (item.carrito != null && item.listaProductos != null) {
					item = new itemCarro(item.carrito, item.listaProductos);
					this.items.push(item);
				}
			}
		} catch (err) {
			// ignore errors while loading...
		}
	}
};

// guardando carro a local storage
store.prototype.saveItems = function() {
	if (localStorage != null && JSON != null) {
		localStorage[this.nombreCarro + "_items"] = JSON.stringify(this.items);
	}
}

// agregar un item(carro)
store.prototype.agregarCarro = function(carrito, carritoProductos) {
	console.log("carrito")
	console.log(carritoProductos);
	// agregar nuevo item
	var item = new itemCarro();
	this.items.push(carrito, carritoProductos);
	// guardar cambios
	this.saveItems();
}

// mostrar carros
store.prototype.mostrarCarros = function() {
	carros = localStorage.getItem("ListaCarro_items");
	carros = JSON.parse(carros);
	for (var i = 0; i < carros.length; i++) {
		car = carros[i];
		console.log(car);
	}
}

// ----------------------------------------------------------------
// carros
// */
function itemCarro(carrito, listaProductos) {
	this.carrito = carrito;
	this.listaProductos = listaProductos;
};