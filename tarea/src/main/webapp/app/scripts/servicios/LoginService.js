'use strict';

angular.module('myApp').factory(
		'LoginService',
		function($resource) {
			var factory = {
				iniciar : $resource('http://localhost:8080/iniciarSesion', {},
						{
							sesion : {
								method : 'GET',
								params : {
									nombreUsuario : '@nombreUsuario',
									clave : '@clave'
								}
							}
						}),
				session : $resource('http://localhost:8080/crearSession', {}, {
					crearSession : {
						method : 'PUT',
						params : {
							nombreUsuario : '@nombreUsuario'
						}
					}
				}),

				registro : $resource('http://localhost:8080/registrarUsuario',
						{}, {
							registrarUsuario : {
								method : 'POST',
								params : {
									nombre : '@nombre',
									apellido : '@apellido',
									username : '@username',
									password : '@password'
								}
							}
						}),
						
				usuarioConectado : $resource(
						'http://localhost:8080/usuarioConectado', {}, {
							obtenerUsuario : {
								method : 'GET',
								params : {
									username : '@username'
								}
							}
						})
			};
			return factory;
		});