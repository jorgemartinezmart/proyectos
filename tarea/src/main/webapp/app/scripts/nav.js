'use strict';

angular.module('myApp').controller('ApCtrl',
		function($scope, $location, $cookies) {
			$scope.usrConectado = {
				nombre : "",
				rol : '',
				estaConectado : '',
				sessionId : ''
			};
			$scope.cantidadRegistros = 0;
			var usr = $cookies.get('usuario');

			if (usr != null) {
				$scope.usrConectado.nombre = usr.nombre;
				$scope.usrConectado.rol = usr.rol;
				$scope.usrConectado.estaConectado = true;
				$scope.usrConectado.sessionId = usr.sessionId;
			}
			;

			$scope.salir = function() {
				$scope.usrConectado = {
					nombre : "",
					rol : '',
					estaConectado : '',
					sessionId : ''
				};
				$cookies.remove('estaConectado');
				$cookies.remove('usuario');
				$cookies.remove('sessionId');
				$location.path('/inicio');
			};

		});
