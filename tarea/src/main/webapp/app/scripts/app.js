'use strict';
angular.module('myApp',
		[ 'ui.router', 'ngRoute', 'ngResource', 'ngCookies', 'pagination' ])

.run(function($rootScope, $location, $cookies) {
	// $rootScope.$on('$routeChangeStart', function(event, next, current) {
	// if ($cookies.get('estaConectado') == false ||
	// $cookies.get('estaConectado') == null) {
	// if(next.templateUrl == 'views/tareas.html' || next.templateUrl ==
	// 'views/empleados.html' ) {
	// $location.path('/login');
	// }
	// }
	// else {
	 var usuario = $cookies.get('usuario');
	// if(next.templateUrl == 'views/inicio.html' || (usuario.rol != 1)) {
	// $location.path('/productos');
	// }
	// }
	// })
}).config(function($routeProvider) {
	$routeProvider.when('/inicio', {
		templateUrl : 'scripts/login/login.html',
		controller : 'LoginController'
	}).when('/productos', {
		templateUrl : 'scripts/productos/listaProductos.tlp.html',
		controller : 'carroController'
	}).when('/carro', {
		templateUrl : 'scripts/productos/carro.htm',
		controller : 'carroController'
	}).when('/home', {
		templateUrl : 'home.html',
		controller : 'ApCtrl'
	}).when('/registro', {
		templateUrl : 'scripts/registrar/registrar.html',
		controller : 'RegistroController'
	}).otherwise({
		redirectTo : '/home'
	});
});