'use strict';

angular.module('myApp')
		.controller(
				'RegistroController',
				function RegistroController($scope, $http, $location, $q, LoginService) {
					var registroPromesa = $q.defer();
					registroPromesa.promise.then(registroUsuario)
					function registroUsuario(reg) {
						if (reg.nombre == null) {
							alert("Usuario ya existe");
							$location.path('/home');
						} else {
							$location.path('/inicio');
						}				
					}
					;

					$scope.registro = function() {
						var reg = LoginService.registro.registrarUsuario({
							nombre : $scope.formRegistro.nombre,
							apellido : $scope.formRegistro.apellido,
							username : $scope.formRegistro.username,
							password : $scope.formRegistro.password
						}).$promise.then(function(reg) {
							registroPromesa.resolve(reg);
						});
					};

				});
