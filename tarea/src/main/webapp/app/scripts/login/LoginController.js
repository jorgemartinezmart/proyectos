'use strict';

angular.module('myApp').controller('LoginController',
		function($scope, $q, LoginService, $log, $cookies, $http, $location) {
			var inicioSesion = $q.defer();

			inicioSesion.promise.then(usrASesion);

			/**
			 * subiendo usuario a session con ngcookies
			 */

			function usrASesion(usr) {
				if (usr.nombre != null) {

					$scope.usrConectado.nombre = usr.nombre;
					$scope.usrConectado.rol = usr.rol;
					$scope.usrConectado.estaConectado = true;
					$scope.usrConectado.sessionId = usr.sessionUser;
					/**
					 * info del objeto usuario
					 */
					$log.info($scope.usrConectado);

					$cookies.put('estaConectado', true);
					$cookies.put('usuario', usr);
					$cookies.put('sessionId');
					// $scope.usrConectado.sessionId = $cookies.JSESSIONID;

					$location.path('/productos');
				} else {
					alert("Usuario no existe");
					$location.path('/registro');
				}
			}
			;

			/**
			 * buscando usuario en bd y consumiendo servicio rest
			 */

			$scope.iniciarSesion = function() {
				var usr = LoginService.iniciar.sesion({
					nombreUsuario : $scope.usuario.nombreUsuario,
					clave : $scope.usuario.clave
				}).$promise.then(function(usr) {
					inicioSesion.resolve(usr);
				});
			};
		});
