package proyectosTinet;

import java.util.Random;
import java.util.Scanner;

public class Functions {

	public static void mostrarMenu(){
		System.out.println("Ingrese opcion de tarea:");
		System.out.println("________________________________________");
		System.out.println("||                                  :) ||");
		System.out.println("||                                     ||");
		System.out.println("||-1) Serie Fibonacci                  ||");
		System.out.println("||-2) Suma numeros naturales           ||");
		System.out.println("||-3) Cantidad de d�gitos de un n�mero ||");
		System.out.println("||-4) Bonus                            ||");
		System.out.println("||-5) Terminar                         ||");		
		System.out.println("||_____________________________________||");		
	}
	
	public static void  ejercicio1(int valor){		
		int a=0;
		int b=1;
		int c=1;								
		
		for(int i=0;i<=valor;i++){
			System.out.println(a);
			c=a+b;
			a=b;
			b=c;
		}		
	}
	
	public static void  ejercicio2(int tope){		
		int suma2=0;		
		for(int j=0; j<=tope;j++){
			suma2=suma2+j;
		}
		System.out.println("El total para el n�mero " + tope + " es: " + suma2);	
	}
	
	public static void  ejercicio3(int valor){		
		String numstring = Integer.toString(valor);
		System.out.println("El largo del n�mero es: " + numstring.length());
	}
	
	public static void ejercicio4(){
		int cont = 0;
		int sup = 1000;
		int inf = 0;
		String valor;
		int random= 0;
		
		do{
			Random rnd = new Random();
			random = (int) Math.floor(Math.random()*(sup-inf+1)+inf);
			System.out.println("Su n�mero es: " + random);
			System.out.println("Si es menor ingrese '<', si mayor ingrese '>', si he adivinado ingrese '='");
			Scanner scan4 = new Scanner(System.in);
			valor = scan4.next();
			
			if(valor.equals("<")){
				sup=random-1;
			}else if(valor.equals(">")){
				inf=random+1;
			}
			cont++;				
		}while(!valor.equals("="));
		
		System.out.println("Su numero es :" + random);
		System.out.println("Fueron necesarias " + cont + " veces para acertar");
	}
	
}
