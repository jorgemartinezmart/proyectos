package proyectosTinet;

import java.util.ArrayList;
import java.util.Scanner;

public class Interpreta {
	
	private String opcion;
	private ArrayList<Documento> lista;
	
	public Interpreta() {		

	}

	public String getOpcion() {
		return opcion;
	}

	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
	
	
	public void interpretaOpcion(String opcion){
		Scanner sc = new Scanner(System.in);
		String tipoArchivo = "";
		String nombre = "";
		if(lista==null){
			lista = new ArrayList<Documento>(); 
		}
		switch (opcion) 
		{					
					case "2" :				  					
						System.out.println("Ingrese tipo de archivo que desea crear:");						
						tipoArchivo = sc.next().toUpperCase();
						switch (tipoArchivo){							
							case "DIGITAL":
									DocDigital digital = new DocDigital();
									digital.crearArchivo();
									lista.add(digital);
									break;				
							case "FISICO":
									DocFisico fisico = new DocFisico();
									fisico.crearArchivo();
									lista.add(fisico);
									break;						
							case "AUDIOVIDEO":
									DocVideoAudio audioVideo = new DocVideoAudio();
									audioVideo.crearArchivo();
									lista.add(audioVideo);
									break;									
						}					
						break;
							
					case "3" :			  					
						System.out.println("Modificar Archivo");
						//agregarCampos(sc.next());
						break;
							
					case "4" :			  					
						System.out.println("Eliminar archivo");
						//agregarRegistros(sc.next());
						
					case "5" :
						System.out.println("Ingrese tipo de documento que desea ver:");
						tipoArchivo = sc.next().toUpperCase();
						switch (tipoArchivo){							
							case "DIGITAL":
									System.out.println("Ingrese nombre autor:");
									nombre = sc.next();
									for(int i=0;i<lista.size();i++){
										if(lista.get(i).getNombreAutor().equals(nombre)){
											lista.get(i).reproducir();											
										}
									}
									break;				
							case "FISICO":
									System.out.println("Ingrese nombre autor:");
									nombre = sc.next();
									DocFisico fisico = new DocFisico();
									fisico.reproducir();
									break;				
							case "AUDIOVIDEO":
									System.out.println("Ingrese nombre autor:");
									nombre = sc.next();
									DocVideoAudio audiovideo = new DocVideoAudio();
									audiovideo.reproducir();
									break;									
						}
						break;
					case "6":
							listarTodo();
							break;
		}		
	}
	
	private void listarTodo(){
			for(int i=0;i<lista.size();i++){			
				System.out.println(i+"-"+lista.get(i).getNombreAutor());			
			}		
	}
}
