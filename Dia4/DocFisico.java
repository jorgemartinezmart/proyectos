package proyectosTinet;

import java.util.Scanner;

public class DocFisico extends Documento {

	private int añoImpresion;
	private String ubicacion;
	private int cantidadPaginas; 
	
	public DocFisico() {
		
	}
	
	public int getAñoImpresion() {
		return añoImpresion;
	}

	public void setAñoImpresion(int añoImpresion) {
		this.añoImpresion = añoImpresion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public int getCantidadPaginas() {
		return cantidadPaginas;
	}

	public void setCantidadPaginas(int cantidadPaginas) {
		this.cantidadPaginas = cantidadPaginas;
	}
	
	public void crearArchivo(){
		super.crearArchivo();
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese año de impresión");
		int año = sc.nextInt();
		System.out.println("Ingrese ubicación");
		String ubicación = sc.next();
		System.out.println("Ingrese cantidad de páginas");
		int cantidad = sc.nextInt();
		setAñoImpresion(año);
		setUbicacion(ubicación);
		setCantidadPaginas(cantidad);
		System.out.println("Documento agregado exitosamente");
	}
	
	public void reproducir(){
		System.out.println("Imposible rerpoducir, por favor dirijase a la direccion: "+getUbicacion() + "  para recuperar la copia y leerla.");
		System.out.println("                                                        											   ");
	    System.out.println(" 									                                    		                       ");
	    System.out.println("                                    									        		               ");
	    
	}
	
}
