package proyectosTinet;

import java.util.Scanner;

public abstract class Documento {
	
	protected String nombreAutor;

	protected Documento() {

	}	
	
	public String getNombreAutor() {
		return nombreAutor;
	}

	public void setNombreAutor(String nombreAutor) {
		this.nombreAutor = nombreAutor;
	}

	protected void reproducir(){}
	
	protected void crearArchivo(){
		System.out.println("Ingrese nombre autor:");
		Scanner sc = new Scanner(System.in);
		this.nombreAutor=sc.next();
	}
}
