package proyectosTinet;

import java.util.Scanner;

public class DocVideoAudio extends Documento {
	
	private String duracionVideo;
	private byte tama�o;
	private String formato;
	
	public DocVideoAudio(){
		
	}

	public String getDuracionVideo() {
		return duracionVideo;
	}

	public void setDuracionVideo(String duracionVideo) {
		this.duracionVideo = duracionVideo;
	}

	public byte getTama�o() {
		return tama�o;
	}

	public void setTama�o(byte tama�o) {
		this.tama�o = tama�o;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	public void crearArchivo(){
		super.crearArchivo();
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese duraci�n video");
		String duracion = sc.next();
		System.out.println("Ingrese tama�o(Byte)");
		byte tama�o = sc.nextByte();
		System.out.println("Ingrese formato (PDF, DOC, TXT, PNG, JPG)");
		String formato = sc.next();
		setDuracionVideo(duracion);
		setTama�o(tama�o);
		setFormato(formato);
		System.out.println("Documento agregado exitosamente");
	}
	
	public void reproducir(){
		System.out.println("Imposible reproducir, solo texto soportado.");		
		System.out.println("                                                          ");
	    System.out.println("                                                          ");
	    System.out.println("                                                          ");
	    
	}
	
}
