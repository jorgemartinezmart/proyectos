package proyectosTinet;

import java.util.Scanner;

public class DocDigital extends Documento{
	
	private int cantidadPaginas;
	private byte tama�o;
	private String formato;
	private String texto;
	
	public DocDigital (){
		
	}
	
	public int getCantidadPaginas() {
		return cantidadPaginas;
	}
	
	public void setCantidadPaginas(int cantidadPaginas) {
		this.cantidadPaginas = cantidadPaginas;
	}

	public byte getTama�o() {
		return tama�o;
	}

	public void setTama�o(byte tama�o) {
		this.tama�o = tama�o;
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void crearArchivo(){
		super.crearArchivo();
		Scanner sc = new Scanner(System.in);
		System.out.println("Ingrese Cantidad de p�ginas");
		int cantidad = sc.nextInt();
		System.out.println("Ingrese formato (PDF, DOC, TXT, PNG, JPG)");
		String formato = sc.next();
		System.out.println("Ingrese tama�o");
		byte tama�o = sc.nextByte();
		System.out.println("Ingrese contenido");
		String texto = sc.next();		
		setCantidadPaginas(cantidad);
		setFormato(formato);
		setTama�o(tama�o);
		setTexto(texto);
		System.out.println("Documento agregado exitosamente");
	}
	
	public void reproducir(){
	    System.out.println("Desplegando el contenido del documento digital escrito.");
	    System.out.println("Autor: "+getNombreAutor());
	    System.out.println("N� P�ginas: "+getCantidadPaginas());
	    System.out.println("Tama�o (byte): "+getTama�o());
	    System.out.println("Formato: "+getFormato());
	    System.out.println("Texto: ");
	    System.out.println(getTexto());
	    System.out.println("                                                          ");
	    System.out.println("                                                          ");
	    System.out.println("                                                          ");
	    
	}
		
}
