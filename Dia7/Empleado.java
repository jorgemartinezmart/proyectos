package com.proyectosTinet.TareaDia7;

import java.util.HashMap;
import java.util.Map;

public class Empleado {

	private int id;
	private String nombre;
	private Map<String, String> caracteristicas = new HashMap<String, String>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Map<String, String> getCaracteristicas() {
		return caracteristicas;
	}
	public void setCaracteristicas(Map<String, String> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
	/*
	@Override
	public String toString() {
		return "ID :" + id + ", Nombre=" + nombre;
	}
	*/
	
}
