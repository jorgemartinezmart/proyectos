package com.proyectosTinet.TareaDia7;

import java.util.Scanner;

import com.proyectosTinet.TareaDia7.Functions;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {        
    	Scanner opciones = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);
		Functions.mostrarMenu();
		String opcion="";
		Organigrama organigrama = new Organigrama();
				
		do{						
				opcion = opciones.next();		
				switch (Integer.parseInt(opcion)) {			
					
					case 1 :			  					
						System.out.println("Mostrar men�");
						Functions.mostrarMenu();
						break;				
					
					case 2 :			  					
						System.out.println("Crear cargo");
						System.out.println("Ingrese nombre del cargo padre: ");
						String cargoPadre = sc.next();
						System.out.println("Ingrese nombre del nuevo cargo: ");
						String nuevoCargo = sc.next();
						organigrama.insertarCargo(cargoPadre, nuevoCargo);;			
						break;
							
					case 3 :			  					
						System.out.println("Insertar empleados");
						System.out.println("Ingrese nombre del cargo: ");
						String nombreCargo = sc.next();
						organigrama.insertaEmpleado(nombreCargo);
						break;
							
					case 4 :			  					
						System.out.println("Mostrando empleados");
						organigrama.mostrarEmpleados();
						break;
					case 5 :			  					
						organigrama.inicializaCargos();
						System.out.println("Inicializando");
						break;	
						
				    case 6 :
						System.out.println("Mostrar Todo");
						organigrama.mostrarCargos();
						break;
						
					case 7 :
						System.out.println("Mostrar Jerarquía");
						Cargo base = organigrama.recuperarCargo("gerente");	
						organigrama.mostrarJerarquia(base,0);
						break;
					case 8 :
						System.out.println("Ordenar empleados por nombre");
						System.out.println("Ingrese nombre del cargo: ");
						String cargo = sc.next();
						organigrama.ordenarListaEmpleados(cargo);
						break;
					case 9 :
						System.out.println("Finalizando");
						break;
						
					default :		
						System.out.println("Opci�n incorrecta, ingrese un n�mero que sea entre 1 y 9");
						
					}	
			}while (!opcion.equals(9));		

	}
}       
