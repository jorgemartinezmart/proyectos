package com.proyectosTinet.TareaDia7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Cargo {
	
	private String nombreCargo;
	private String Hijode;
	private List<Cargo> hijos;
	private List<Empleado> empleado = new ArrayList<Empleado>();
	
	public String getNombreCargo() {
		return nombreCargo;
	}


	public void setNombreCargo(String nombreCargo) {
		this.nombreCargo = nombreCargo;
	}


	public String getHijode() {
		return Hijode;
	}


	public void setHijode(String Hijode) {
		this.Hijode = Hijode;
	}


	public List<Empleado> getEmpleado() {
		return empleado;
	}



	public void setEmpleado(List<Empleado> empleado) {
		this.empleado = empleado;
	}



	public void insertarEmpleado(String cargo, Empleado e){
		Scanner sc = new Scanner(System.in);
		System.out.print("Ingrese nombre empleado:");	
		e.setNombre(sc.next());
		System.out.print("Ingrese id:");	
		e.setId(sc.nextInt());
		
		System.out.print("Desea agregar caracteristicas? (S/N):");	
		String respuesta=sc.next();
		
		if(respuesta.toUpperCase().equals("S")){
			Map<String, String> caracteristicas = new HashMap<String, String>();
			String seguir="";
				do{
					System.out.print("Ingrese nombre caracteristica:");
					String clave = sc.next();
					System.out.print("Ingrese valor caracteristica:");
					String valor = sc.next();				
					caracteristicas.put(clave,valor);
					System.out.print("Desea seguir agregando caracteristicas? (S/N):");	
					seguir = sc.next();
				}while(seguir.toUpperCase().equals("S"));
				e.setCaracteristicas(caracteristicas);	
		//
		}
		empleado.add(e);
	}

/*
	@Override
	public String toString() {
		return "Hijo de :" + Hijode
				+ ", empleado=" + empleado.size();
	}
*/

	public List<Cargo> getHijos() {
		return hijos;
	}


	public void setHijos(List<Cargo> hijos) {
		this.hijos = hijos;
	}
	
	

}
