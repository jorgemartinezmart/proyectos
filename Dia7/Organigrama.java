package com.proyectosTinet.TareaDia7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Organigrama {

	private Map<String, Cargo> cargos; 
	
	
	public void insertarCargo(String cargoPadre, String nuevoCargo){		
		try{
			if(cargos==null){
				cargos= new HashMap<String, Cargo>(); 
			}
			if(cargos.containsKey(nuevoCargo)){
				System.out.println("Ya existe");
			}else{
				//llenado nuevo objecto 
				Cargo cargo11 = new Cargo();
				cargo11.setNombreCargo(nuevoCargo);
				cargos.put(nuevoCargo, cargo11);
				
				//enlazando padre a hijo				
				
				Cargo padre = recuperarCargo(cargoPadre);				
				List<Cargo> listadeHijos = padre.getHijos();				
				if(listadeHijos == null){
					listadeHijos = new ArrayList<Cargo>();
				}
				
				listadeHijos.add(cargo11);
				padre.setHijos(listadeHijos);				
				System.out.println("Cargo ingresado");
			}
		} catch (Exception e){
			System.out.println("Error " + e);}
	}
	
	
	public Cargo recuperarCargo(String nombreCargo){
		return cargos.get(nombreCargo);		
	}
	
	public void insertaEmpleado(String nombreCargo){
		Empleado empleados = new Empleado();
		Cargo cargo = recuperarCargo(nombreCargo);	
		cargo.insertarEmpleado(nombreCargo, empleados);
	}
	
	public void mostrarCargos(){
		 for(Map.Entry c:cargos.entrySet()){  
			   System.out.println("Nombre cargo: " +c.getKey() + " "+ cargos.get(c.getKey()).getHijode());  
		 }  
	}
	
	public void mostrarEmpleados(){
		Cargo cargo = null;
		List<Empleado> empleados = new ArrayList<Empleado>(); 
		for(Map.Entry<String, Cargo> c:cargos.entrySet()){			   
			   cargo = recuperarCargo(c.getKey().toString());
		       empleados = cargo.getEmpleado();	
		       if(empleados.size()==0){
		    	   System.out.println("No existen empleados para el cargo "+c.getKey());
		       }else{
		       for(int i=0;i<empleados.size();i++){			
					System.out.println("Empleado :"+empleados.get(i).getId());
					System.out.println("Nombre :"+empleados.get(i).getNombre());
					System.out.println("Cargo: " +c.getKey());
					System.out.println("Datos Adicionales :"+empleados.get(i).getCaracteristicas());
				}
		       }
		 }
	}

	public void inicializaCargos(){
		if(cargos==null){
			cargos= new HashMap<String, Cargo>(); 
		}
		Cargo cargo = new Cargo();
		cargo.setNombreCargo("gerente");
		cargos.put("gerente", cargo);
	}

	
	public void mostrarJerarquia(Cargo cargo,int largo){
		List<Cargo> hijos = new ArrayList<Cargo>();
		hijos = cargo.getHijos();
		String concatenar="";
		for(int j=0;j<largo;j++){
			concatenar=concatenar + "   ";
		}
		if(!(hijos==null)){
			for(int i=0;i<hijos.size();i++){
				System.out.println(concatenar+"<"+hijos.get(i).getNombreCargo().toUpperCase());
				System.out.println(concatenar+"-----------");
				largo++;
				mostrarJerarquia(recuperarCargo(hijos.get(i).getNombreCargo()), largo);
			}
		}		
	}
	
	public void ordenarListaEmpleados(String nombreCargo){
	  	Cargo cargo = recuperarCargo(nombreCargo);
	  	List<Empleado> listaEmpleados = cargo.getEmpleado();
	  	Collections.sort(listaEmpleados, new Ordenamiento());	  	
	  	for(int i=0; i < listaEmpleados.size();i++){
	  		System.out.println(listaEmpleados.get(i).getNombre());
	  	}
	  	//Comparator<String> c = Comparator.comparing(String::toString); JAVA 8
	 }
}

