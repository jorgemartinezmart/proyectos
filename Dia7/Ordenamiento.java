package com.proyectosTinet.TareaDia7;

import java.util.Comparator;

public class Ordenamiento implements Comparator<Empleado>{

	public int compare(Empleado e1, Empleado e2) {
		return e1.getNombre().compareTo((e2.getNombre()));
	}
	
	
}
