package proyectosTinet;

import java.util.Scanner;

import proyectosTinet.*;

public class App {

	public static void main(String[] args) {
		
		Scanner opciones = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);
		Tabla tabla = new Tabla();
		Funciones.mostrarMenu();
		String opcion;
		//lista.mostrarMenu();
		int posicion=0;
		int valor=0;
		String nombreTabla="";
				
		do{						
				opcion = opciones.next();		
				switch (opcion) {			
				
				case "1" :			  					
					System.out.println("Mostrar men�");
					Funciones.mostrarMenu();
					break;				
				
				case "2" :			  					
						System.out.println("Ingrese nombre de tabla");
						nombreTabla = sc.next();	
						tabla.crearTabla(nombreTabla);
						break;
						
				case "3" :			  					
						System.out.println("Ingrese nombre de tabla, a la cual desea agregar campos");
						nombreTabla = sc.next();
						tabla.agregarCampos(nombreTabla);
						break;
					
				case "4" :			  					
						System.out.println("Ingrese nombre de tabla, a la cual desea agregar registros");
						nombreTabla = sc.next();
						tabla.agregarRegistros(nombreTabla);
						break;
				case "5" :
						System.out.println("Ingrese nombre de tabla de la cual desea ver registros");
						nombreTabla = sc.next();
						tabla.verRegistrosPorTabla(nombreTabla);
						break;
				case "6": 
						tabla.verRegistros();
						break;				
				case "7":
					System.out.println("Ha finalizado la aplicaci�n");
					break;
				default :		
						System.out.println("Opci�n incorrecta, ingrese un n�mero que sea entre 1 y 7");
				}	
		}while (!opcion.equals("7"));
		
	}

}

