package proyectosTinet;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class Tabla {

	private String nombreTabla;
	private HashMap<String, String>  registros;
	private HashMap<String, Object>  campos;
	private HashMap <String, Object> tablas;
	private Scanner sc = new Scanner(System.in);

	public Tabla(HashMap<String,String> campos) {
		this.campos = new HashMap();
	}

	public Tabla() {
		this.nombreTabla = nombreTabla;
	}

	public String getNombreTabla() {
		return nombreTabla;
	}

	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}

	public HashMap<String, Object> getCampos() {
		return campos;
	}

	public void setCampos(HashMap<String, Object> campos) {
		this.campos = campos;
	}

	public HashMap<String, Object> getTablas() {
		return tablas;
	}

	public void setTablas(HashMap<String, Object> tablas) {
		this.tablas = tablas;
	}

	public void crearTabla(String nombreTabla){
		if(tablas==null){
			tablas = new HashMap<String, Object>();
		}
		if(tablas.containsKey(nombreTabla)){
			System.out.println("Error, tabla ya existe");
		}else{
			HashMap<String, Object> c = new HashMap<String, Object>();			
			tablas.put(nombreTabla, c);
			System.out.println("Tabla "+tablas.keySet() +" agregada");
		}

	}	
	
	public void agregarCampos(String nombreTabla){
		if(tablas==null){
			System.out.println("Error, no se han creado tablas, seleccione opci�n 1");
		}else{										
				if(tablas.containsKey(nombreTabla)){
					if(campos==null){
						campos = new HashMap<String, Object>();
					}	//validar si existe campo					        
						        HashMap<String, String> reg = new HashMap<String, String>();
								boolean continuar = false;	
								int i=0;
								System.out.println("Ingrese nombre del campo: ");
								String nombreCampo = sc.next();									
								HashMap<String, Object> c = new HashMap<String, Object>();
								c.put(nombreCampo,reg);
								for(Map.Entry t:tablas.entrySet()){  
									   if(t.getKey()==nombreTabla){										   
										   tablas.put(nombreTabla,c);
									   }  
									  } 
								campos.put(nombreTabla+"-"+nombreCampo, c);					
					}else{
						System.out.println("Error, tabla no existe");
					}
			}
	}
	
	public void agregarRegistros(String nombreTabla){
		if(tablas==null){
			System.out.println("Error, no se han creado tablas, seleccione opci�n 1");
		}else{										
				if(tablas.containsKey(nombreTabla)){
					if(campos==null){
						campos = new HashMap<String, Object>();
					}	
					if(registros==null){
						registros = new HashMap<String, String>();
					}
						System.out.println("Ingrese nombre del campo: al cual desea agregar registro");
						String nombreCampo = sc.next();	
						/* validar si existe campo  y registro if(){
							
						}*/
						        HashMap<String, String> reg = new HashMap<String, String>();
								boolean continuar = false;	
								int i=0;
								System.out.println("Ingrese nombre del valor del registro: ");
								String valorRegistro = sc.next();								
								reg.put(nombreTabla+"-"+nombreCampo,valorRegistro);
								
								for(Map.Entry t:tablas.entrySet()){  
									   if(t.getKey()==nombreTabla){		
										   for(Map.Entry c:campos.entrySet()){  
											   String[] partes = ((String) c.getKey()).split("-");
											   if(partes[1]==nombreCampo){										   
												   campos.put(nombreTabla+"-"+nombreCampo,reg);
												   break;
											   }  
										} 
									   }
									   
								} 
								registros.put(nombreTabla+"-"+nombreCampo,valorRegistro);						
					}else{
						System.out.println("Error, tabla no existe");
					}
			}
	}
	
	public void verRegistros(){
		if(tablas==null){
			System.out.println("No se han creado tablas");			
		}else{
			
		  for(Map.Entry t:tablas.entrySet()){				
				   System.out.println("Tabla: "+t.getKey()); 
				   for(Map.Entry c:campos.entrySet()){
					   String[] partes = ((String) c.getKey()).split("-");
					   if(partes[0].equals(t.getKey())){
						   System.out.println("Campo: "+c.getKey());
					   }					   
					   for(Map.Entry r:registros.entrySet()){ 
						   String[] part = ((String) r.getKey()).split("-");
						   String[] ca = ((String) c.getKey()).split("-");
						   if(part[0].equals(t.getKey()) && part[1].equals(ca[1])){
							   System.out.println("Registro: "+r.getValue());
						   }					 					  
						  }
					  }				   
				   System.out.println("-----------------------");
				   System.out.println("-----------------------");
			}  
		}
		
	}
	
	public void verRegistrosPorTabla(String nombreTabla){
		if(tablas==null){
			System.out.println("No se han creado tablas");			
		}else{
			  for(Map.Entry t:tablas.entrySet()){  
				   System.out.println("Tabla: "+t.getKey());
				   for(Map.Entry c:campos.entrySet()){  
					   System.out.println(c.getKey());  
					  }  
				   System.out.println("-----------------------");
				   System.out.println("-----------------------");
				  }  
		}
		
	}
	
}
