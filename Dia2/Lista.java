package proyectosTinet;

public class Lista {

	private ObjetosParaLista inicio;
	private int largoLista;
	
	public Lista(){
		inicio = null;
		largoLista = 0;
	}
	
	public boolean esVacia(){
		return inicio ==null;
	}
	
	public int getLargoLista(){
		return largoLista;
	}
	
	/**
	 * Muestra el men� de la aplicaci�n y los nodos existentes.  
	 */
	 
	public void mostrarMenu(){
		System.out.println("Ingrese opcion de tarea:");
		System.out.println("______________________________________________");
		System.out.println("||                                           ||");
		System.out.println("||                                           ||");
		System.out.println("||-1) Mostrar men�                         . ||");
		System.out.println("||-2) Agregar elemento al final de la lista. ||");
		System.out.println("||-3) Obtener elemento a partir de posici�n. ||");
		System.out.println("||-4) Eliminar elemento.                     ||");
		System.out.println("||-5) Insertar elemento en posici�n.         ||");
		System.out.println("||-6) Obtener cantidad de elementos.         ||");
		System.out.println("||-7) Mostrar lista completa.                ||");
		System.out.println("||-8) Terminar                               ||");
		System.out.println("||___________________________________________||");
	}	
	
	/**
	 * Muestra la lista completa con sus valores correspondientes al usuario para que �ste sepa
	 * los valores de cada nodo.  
	 */
	 
	public void mostrarLista(){
		if(!esVacia()){
			ObjetosParaLista aux = inicio;
			int i = 0;
			while(aux!=null){
				System.out.println("Posici�n:" + i + " valor: " + aux.getValor());
				aux = aux.getSiguiente();
				i++;
			}
		}else{
			System.out.println("Lista vac�a");
		}
		
	}
	
	/**
	 * Agrega un nodo al final de la lista, se valida si �sta est� vac�a agregandola al comienzo,
	 * de lo contrario, se recorre la lista obteniendo la �ltima posici�n y se inserta el nodo luego
	 * de �sta.  
	 * @param valor es el valor que se agrega a la lista
	 */
	public void agregarAlFinal(int valor){		
		ObjetosParaLista nuevo = new ObjetosParaLista();
		nuevo.setValor(valor);
		
		if(esVacia()){			
			inicio = nuevo;
			System.out.println("Elemento agregado exitosamente!");
		}else{
			ObjetosParaLista aux = inicio;
			while(aux.getSiguiente() != null){
			    aux = aux.getSiguiente();	
			}			
			aux.setSiguiente(nuevo);
			System.out.println("Elemento agregado exitosamente!");
		}
		largoLista++;		
	}
	
	/**
	 * Muestra la lista completa con sus valores correspondientes al usuario para que �ste sepa
	 * los valores de cada nodo.  
	 * @param posicion es la posicion del elemento a obtener
	 */
	 
	public int getElemento(int posicion){		
		if(posicion>=0 && posicion<largoLista){			
			if(posicion == 0){
				return inicio.getValor();
			}else{
				ObjetosParaLista aux = inicio;
				for(int i =0;i<posicion;i++){					
					aux = aux.getSiguiente();
				}
				return aux.getValor();			
			}			
		}
		else{
			System.out.println("Posici�n inexistente");
			return posicion;
		}
	}

	/**
	 * Elimina un nodo a partir de una posici�n entregada.  
	 * @param posicion es la posici�n del elemento a eliminar.
	 */
	public void eliminarElemento(int posicion){		
		if(posicion>=0 && posicion<largoLista){			
			if(posicion == 0){
				inicio = inicio.getSiguiente();
				System.out.println("Elemento agregado exitosamente!");
			}else{
				ObjetosParaLista aux = inicio;				
				for(int i =0;i<posicion-1;i++){					
					aux = aux.getSiguiente();
				}				
				ObjetosParaLista siguiente = aux.getSiguiente();	
				aux.setSiguiente(siguiente.getSiguiente());
				System.out.println("Elemento eliminado!");
			}	
			largoLista--;
		}
		else{
			System.out.println("Posici�n inexistente");
		}
	}
	
	/**
	 * Inserta un elemento en la posicion dada.  
	 * @param posicion es la posici�n donde se debe insertar.
	 * @param valor es el valor del elemento a insertar.
	 */
	public void insertarElemento(int posicion, int valor){		
		if(posicion>=0 && posicion<largoLista){			
			ObjetosParaLista nuevo = new ObjetosParaLista();
			nuevo.setValor(valor);
			if(posicion == 0){
				nuevo.setSiguiente(inicio);
				inicio=nuevo;
				System.out.println("Elemento "+valor+" insertado en posicion "+ posicion +" exitosamente!");
			}else{
				 	if(posicion == largoLista){
					ObjetosParaLista aux = inicio;
					while(aux.getSiguiente() != null){
						aux = aux.getSiguiente();
					}					
					aux.setSiguiente(nuevo);
					System.out.println("Elemento "+valor+" insertado en posicion "+ posicion +" exitosamente!");
				 	}else{
						ObjetosParaLista aux = inicio;
						for(int i=0;i<(posicion-1);i++){
							aux = aux.getSiguiente();
						}
						ObjetosParaLista siguiente = aux.getSiguiente();
						aux.setSiguiente(nuevo);
						nuevo.setSiguiente(siguiente);
						System.out.println("Elemento "+valor+" insertado en posicion "+ posicion +" exitosamente!");
				 	}				
				}				
			largoLista++;
		}
		else{			
			System.out.println("Posici�n inexistente, revise el largo de la lista");
		}
	}	
	
}
