package proyectosTinet;

import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		
		Scanner opciones = new Scanner(System.in);
		Scanner sc = new Scanner(System.in);
		Lista lista = new Lista();
		String opcion;
		lista.mostrarMenu();
		int posicion=0;
		int valor=0;
			
		do{						
				opcion = opciones.next();		
				switch (opcion) {
			
				case "1" :			  					
						lista.mostrarMenu();				  	
						break;
				case "2" :			  					
					  	System.out.println("Ingrese un valor para agregar al final de la lista:");
					  	valor = sc.nextInt();	
					  	lista.agregarAlFinal(valor);
						break;
				case "3":
						System.out.println("Ingrese una posici�n para buscar:");
						posicion = sc.nextInt();
						int elemento = lista.getElemento(posicion);
						System.out.println("El elemento de la posici�n "+posicion+" es "+elemento);
						break;
				case "4":
						System.out.println("Ingrese una posici�n para eliminar:");
						posicion = sc.nextInt();	
						lista.eliminarElemento(posicion);
						break;
				case "5":
						System.out.println("Ingrese posici�n donde se insert� el elemento");
						posicion = sc.nextInt();
						System.out.println("Ingrese valor para el elemento:");
						valor = sc.nextInt();	
						lista.insertarElemento(posicion,valor);
						break;						
				case "6":
						System.out.println("La cantidad de elementos es: "+lista.getLargoLista());
						break;
						
				case "7": 
						lista.mostrarLista();
						break;				
				case "8":
					System.out.println("Ha finalizado la aplicaci�n");
					break;
				default :		
						System.out.println("Opci�n incorrecta, ingrese un n�mero que sea entre 1 y 8");
				}	
		}while (!opcion.equals("8"));
		
	}

}
