package proyectosTinet;

public class ObjetosParaLista {

	private int valor;
	private ObjetosParaLista siguiente;
	
	public ObjetosParaLista() {
		this.valor = 0;
		this.siguiente = null;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public ObjetosParaLista getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(ObjetosParaLista siguiente) {
		this.siguiente = siguiente;
	}
	
	
	
}
